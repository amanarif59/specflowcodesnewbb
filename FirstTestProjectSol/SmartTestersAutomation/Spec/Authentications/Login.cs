﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace SmartTestersAutomation.Spec.Authentications
{
    [Binding]
    public sealed class Login : Steps
    {
        [Given(@"I navigate to ""(.*)""")]
        public void GivenINavigateTo(string url)
        {
                        
        }

        [Given(@"I click ""(.*)""")]
        [When(@"I click login button")]
        public void GivenIClickLoginLink(string locator)
        {
            
        }

        [When(@"I enter ""(.*)"" as ""(.*)""")]
        public void WhenIEnterUsernameAsUser(int locator, string data)
        {
            
        }
         

        [Then(@"login is ""(.*)""")]
        public void ThenLoginIsSuccessful()
        {
          
        }

    }
}
