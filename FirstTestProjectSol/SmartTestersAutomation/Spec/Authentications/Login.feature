﻿Feature: Login
	In order to perform various actions
	As a client
	I want to be logged into Smart Testers training portal

@Regression
Scenario: Verify Login functionality
	Given I navigate to "http://localhost/smarttesters/"
	And I click loginLink
	When I enter username as user1
	And I enter password as pass1
	And I click loginButton
	Then login is successful
